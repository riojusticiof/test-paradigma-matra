@include('theme.header')

<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <!-- Navbar -->
        @include('theme.navbar')
        <!-- END Navbar -->

        <!-- Sidebar -->
        @include('theme.sidebar')
        <!-- END Sidebar -->

        <!-- Main Content -->
        @if (\Session::has('flashMessage'))
        <div class="show-alert shadow p-3 mb-5 rounded">
            <div class="alert alert-{!! session('flashType') !!} mb-0" role="alert">{!! session('flashMessage') !!}</div>
        </div>
        @endif
        @yield('content')

    </div>
</div>

<!-- Footer -->
@include('theme.footer')
<!-- END Footer -->

<!-- Script -->
@include('theme.script')
<!-- END Script -->