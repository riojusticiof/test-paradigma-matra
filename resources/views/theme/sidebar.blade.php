<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li><a class="nav-link" href="{{ url('/') }}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
            <li class="menu-header">Starter</li>
            <li><a class="nav-link" href="{{ url('companies') }}"><i class="fas fa-building"></i> <span>Companies</span></a></li>
            <li><a class="nav-link" href="{{ url('employees') }}"><i class="fas fa-users"></i> <span>Employees</span></a></li>
        </ul>
    </aside>
</div>