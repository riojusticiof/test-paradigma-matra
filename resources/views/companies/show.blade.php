@extends('theme.theme')
@section('title', 'Companies')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Companies</h1>
        </div>

        <div class="card">

            <div class="card-body">
                <div class="mb-3">
                    <a href="{{ url('companies') }}" class="btn btn-primary">Back</a>
                </div>

                <div class="mb-3 row">
                    <label for="name" class="col-md-2 col-form-label">Name</label>
                    <div class="col-md-10">
                        <input class="form-control" name="name" type="text" placeholder="lorem_ipsum" id="name" value="{{ $companies->name }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="email" class="col-md-2 col-form-label">Email</label>
                    <div class="col-md-10">
                        <input class="form-control" name="email" type="text" placeholder="lorem@ipsum.com" id="email" value="{{ $companies->email }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="logo" class="col-md-2 col-form-label">Logo</label>
                    <div class="col-md-10">
                        <img src="{{ url('/') }}/storage/{{ $companies->logo }}" alt="logo" id="loog">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="website" class="col-md-2 col-form-label">Website</label>
                    <div class="col-md-10">
                        <input class="form-control" name="website" type="text" placeholder="lorem_ipsum.com" id="website" value="{{ $companies->website }}" />
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
@endSection