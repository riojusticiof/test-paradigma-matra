@extends('theme.theme')
@section('title', 'Edit Companies')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Companies</h1>
        </div>

        <div class="card">

            <div class="card-body">
                <form action="{{ url('companies') }}/{{ $companies->id }}" method="POST" enctype="multipart/form-data">
                    @CSRF
                    @method('put')
                    <input type="hidden" name="old_logo" value="{{ $companies->logo }}">
                    <!-- HTML5 Inputs -->
                    <div class="mb-3 row">
                        <label for="name" class="col-md-2 col-form-label">Name</label>
                        <div class="col-md-10">
                            <input class="form-control" name="name" type="text" id="name" value="{{ $companies->name }}" />
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="email" class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                            <input class="form-control" name="email" type="text" placeholder="lorem@ipsum.com" id="email" value="{{ $companies->email }}" />
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="logo" class="col-md-2 col-form-label text-nowrap">Logo</label>
                        <div class="col-md-10">
                            <label for="logo" class="col-md-2 col-form-label text-nowrap text-danger">* dimensions: 100x100</label>
                            <input class="form-control" type="file" id="logo" name="logo" data-max-file-size="2M">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="website" class="col-md-2 col-form-label">Website</label>
                        <div class="col-md-10">
                            <input class="form-control" name="website" type="text" placeholder="lorem_ipsum.com" id="website" value="{{ $companies->website }}" />
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ url('companies') }}" class="btn btn-warning">Back</a>
                </form>
            </div>

        </div>
    </section>
</div>
@endSection