@extends('theme.theme')
@section('title', 'Companies')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Companies</h1>
        </div>

        <div class="card">

            <div class="card-body">
                <div class="mb-3">
                    <a href="{{ url('companies', 'create') }}" class="btn btn-primary">Add</a>
                </div>

                <div class="table-responsive">
                    <table id="datatable" class="table stripe">
                        <thead class="text-nowrap">
                            <tr>
                                <th>Option</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Website</th>
                            </tr>
                        </thead>
                        <tbody class="table-border-bottom-0">
                            @foreach($companies as $val)
                            <tr>
                                <td>
                                    <a href="{{ url('companies') }}/{{ $val->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                                    <a href="{{ url('companies') }}/{{ $val->id }}" class="btn btn-sm btn-info">Show</a>
                                    <form action="{{ url('companies') }}/{{ $val->id }}" method="POST">
                                        @CSRF
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                                <td>{{ $val->name }}</td>
                                <td>{{ $val->email }}</td>
                                <td>
                                    <img src="{{ url('/') }}/storage/{{ $val->logo }}" alt="logo">
                                </td>
                                <td>
                                    <a href="{{ $val->website }}">{{ $val->website }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <div class="d-flex justify-content-center">{{ $companies->links() }}</div>

                </div>
            </div>

        </div>
    </section>
</div>
@endSection