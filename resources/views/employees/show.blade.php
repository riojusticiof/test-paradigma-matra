@extends('theme.theme')
@section('title', 'Employees')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Employees</h1>
        </div>

        <div class="card">

            <div class="card-body">
                <div class="mb-3">
                    <a href="{{ url('employees') }}" class="btn btn-primary">Back</a>
                </div>

                <div class="mb-3 row">
                    <label for="first_name" class="col-md-2 col-form-label">Employee First Name</label>
                    <div class="col-md-10">
                        <input class="form-control" name="first_name" type="text" placeholder="lorem_ipsum" id="first_name" readonly value="{{ $employees->first_name }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="last_name" class="col-md-2 col-form-label">Employee Last Name</label>
                    <div class="col-md-10">
                        <input class="form-control" name="last_name" type="text" placeholder="lorem_ipsum" id="last_name" readonly value="{{ $employees->last_name }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="name" class="col-md-2 col-form-label">Company Name</label>
                    <div class="col-md-10">
                        <input class="form-control" name="name" type="text" placeholder="lorem_ipsum.com" id="name" readonly value="{{ $employees['empCompanies']->name }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="email" class="col-md-2 col-form-label">Company Email</label>
                    <div class="col-md-10">
                        <input class="form-control" name="email" type="text" placeholder="lorem_ipsum.com" id="email" readonly value="{{ $employees['empCompanies']->email }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="logo" class="col-md-2 col-form-label">Company Logo</label>
                    <div class="col-md-10">
                        <img src="{{ url('/') }}/storage/{{ $employees['empCompanies']->logo }}" alt="logo" id="loog">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="website" class="col-md-2 col-form-label">Company Website</label>
                    <div class="col-md-10">
                        <input class="form-control" name="website" type="text" placeholder="lorem_ipsum.com" id="website" readonly value="{{ $employees['empCompanies']->website }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="email" class="col-md-2 col-form-label">Employee Email</label>
                    <div class="col-md-10">
                        <input class="form-control" name="email" type="text" placeholder="lorem@ipsum.com" id="email" readonly value="{{ $employees->email }}" />
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="phone" class="col-md-2 col-form-label">Employee Phone</label>
                    <div class="col-md-10">
                        <input class="form-control" name="phone" type="text" placeholder="lorem@ipsum.com" id="phone" readonly value="{{ $employees->phone }}" />
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
@endSection