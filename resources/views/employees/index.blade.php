@extends('theme.theme')
@section('title', 'Employees')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Employees</h1>
        </div>

        <div class="card">

            <div class="card-body">
                <div class="mb-3">
                    <a href="{{ url('employees', 'create') }}" class="btn btn-primary">Add</a>
                </div>

                <div class="table-responsive">
                    <table id="datatable" class="table stripe">
                        <thead class="text-nowrap">
                            <tr>
                                <th>Option</th>
                                <th>Employee First Name</th>
                                <th>Employee Last Name</th>
                                <th>Company Name</th>
                                <th>Company Email</th>
                                <th>Company Logo</th>
                                <th>Company Website</th>
                                <th>Employee Email</th>
                                <th>Employee Phone</th>
                            </tr>
                        </thead>
                        <tbody class="table-border-bottom-0">
                            @foreach($employees as $val)
                            <tr>
                                <td>
                                    <a href="{{ url('employees') }}/{{ $val->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                                    <a href="{{ url('employees') }}/{{ $val->id }}" class="btn btn-sm btn-info">Show</a>
                                    <form action="{{ url('employees') }}/{{ $val->id }}" method="POST">
                                        @CSRF
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                                <td>{{ $val->first_name }}</td>
                                <td>{{ $val->last_name }}</td>
                                <td>{{ $val['empCompanies']->name }}</td>
                                <td>{{ $val['empCompanies']->email }}</td>
                                <td>
                                    <img src="{{ url('/') }}/storage/{{ $val->logo }}" alt="logo">
                                </td>
                                <td>
                                    <a href="{{ $val->website }}">{{ $val->website }}</a>
                                </td>
                                <td>{{ $val->email }}</td>
                                <td>{{ $val->phone }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <div class="d-flex justify-content-center">{{ $employees->links() }}</div>

                </div>
            </div>

        </div>
    </section>
</div>
@endSection