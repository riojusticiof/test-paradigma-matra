@extends('theme.theme')
@section('title', 'Create Employees')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Employees</h1>
        </div>

        <div class="card">

            <div class="card-body">
                <form action="{{ url('employees') }}" method="POST" enctype="multipart/form-data">
                    @CSRF
                    <!-- HTML5 Inputs -->
                    <div class="mb-3 row">
                        <label for="first_name" class="col-md-2 col-form-label">First Name</label>
                        <div class="col-md-10">
                            <input class="form-control" name="first_name" type="text" placeholder="lorem_ipsum" id="first_name" />
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="last_name" class="col-md-2 col-form-label">Last Name</label>
                        <div class="col-md-10">
                            <input class="form-control" name="last_name" type="text" placeholder="lorem_ipsum" id="last_name" />
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="companies_id" class="col-md-2 col-form-label">Companies</label>
                        <div class="col-md-10">
                            <select class="form-control select2" name="companies_id" id="companies_id">
                                <option value="" selected disabled>Choose Companies</option>
                                @if (!empty($companies))
                                @foreach ($companies as $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="email" class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                            <input class="form-control" name="email" type="text" placeholder="lorem@ipsum.com" id="email" />
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="phone" class="col-md-2 col-form-label">Phone</label>
                        <div class="col-md-10">
                            <input class="form-control" name="phone" type="text" placeholder="081234567890" id="phone" />
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ url('employees') }}" class="btn btn-warning">Back</a>
                </form>
            </div>

        </div>
    </section>
</div>
@endSection