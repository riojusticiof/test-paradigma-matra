<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return view('index');
        }
        return view('auth.login');
    }

    public function dologin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', 'Email or password required.');
            return redirect()->to('/auth/login')->withInput();
        }

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->to('/');
        }

        if ($validator->fails()) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', 'Email or password doesn\'t match.');
            return redirect()->to('/auth/login')->withInput();
        }
    }

    public function dologout()
    {
        Auth::logout();
        return redirect()->to('/auth/login');
    }

    public function dashboard()
    {
        return view('index');
    }
}
