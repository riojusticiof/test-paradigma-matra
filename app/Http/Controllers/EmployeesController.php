<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getEmployees = Employees::with(['empCompanies' => function($q) {
            $q->get();
        }])->paginate(10);

        $data['employees'] = $getEmployees;
        return view('employees.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getCompanies = Companies::all();

        $data['companies'] = $getCompanies;
        return view('employees.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataInputValidation = [
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
        ];
        $dataValidate = [
            'first_name' => 'required',
            'last_name'  => 'required',
        ];
        $validator = Validator::make($dataInputValidation, $dataValidate);

        if ($validator->fails()) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $validator->errors());
            return redirect()->back()->withInput($request->all());
        }

        try {
            $data = [
                'first_name'   => $request->first_name,
                'last_name'    => $request->last_name,
                'companies_id' => $request->companies_id,
                'email'        => $request->email,
                'phone'        => $request->phone,
                'created_at'   => date('Y-m-d H:i:s'),
            ];
            Employees::create($data);
            Session::flash('flashType', 'success');
            Session::flash('flashMessage', 'Failed create employees');
            return redirect()->to('/employees');
        } catch (\Throwable $th) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $th->getMessage());
            // Session::flash('flashMessage', 'Failed create employees');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getEmployees = Employees::with(['empCompanies' => function($q) {
            $q->get();
        }])->first();
        
        $data['employees'] = $getEmployees;
        return view('employees.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getEmployees = Employees::where('id', $id)->first();
        $getCompanies = Companies::all();

        $data['employees'] = $getEmployees;
        $data['companies'] = $getCompanies;
        return view('employees.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataInputValidation = [
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
        ];
        $dataValidate = [
            'first_name' => 'required',
            'last_name'  => 'required',
        ];
        $validator = Validator::make($dataInputValidation, $dataValidate);

        if ($validator->fails()) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $validator->errors());
            return redirect()->back()->withInput($request->all());
        }

        try {
            $data = [
                'first_name'   => $request->first_name,
                'last_name'    => $request->last_name,
                'companies_id' => $request->companies_id,
                'email'        => $request->email,
                'phone'        => $request->phone,
                'updated_at'   => date('Y-m-d H:i:s'),
            ];
            Employees::where('id', $id)->update($data);
            Session::flash('flashType', 'success');
            Session::flash('flashMessage', 'Success update employees');
            return redirect()->to('/employees');
        } catch (\Throwable $th) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $th->getMessage());
            // Session::flash('flashMessage', 'Failed update employees');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Employees::where('id', $id)->delete();
            Session::flash('flashType', 'success');
            Session::flash('flashMessage', 'Success delete employees');
            return redirect()->to('/employees');
        } catch (\Throwable $th) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $th->getMessage());
            // Session::flash('flashMessage', 'Failed delete employees');
            return redirect()->back()->withInput();
        }
    }
}
