<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getCompanies = Companies::paginate(5);

        $data['companies'] = $getCompanies;
        return view('companies.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataInputValidation = [
            'name' => $request->name,
            'logo' => $request->logo,
        ];
        $dataValidate = [
            'name' => 'required',
            'logo' => 'image|dimensions:width=100,height=100'
        ];
        $validator = Validator::make($dataInputValidation, $dataValidate);

        if ($validator->fails()) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $validator->errors());
            return redirect()->back()->withInput($request->all());
        }

        $filename = '';
        if (!empty($request->file('logo'))) {
            $file = $request->file('logo');
            $filename = date('YmdHis') . '-' . $file->getClientOriginalName();
            Storage::putFileAs('public', $file, $filename);
        }

        try {
            $data = [
                'name'       => $request->name,
                'email'      => $request->email,
                'logo'       => $filename,
                'website'    => $request->website,
                'created_at' => date('Y-m-d H:i:s'),
            ];
            Companies::create($data);
            Session::flash('flashType', 'success');
            Session::flash('flashMessage', 'Failed create companies');
            return redirect()->to('/companies');
        } catch (\Throwable $th) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $th->getMessage());
            // Session::flash('flashMessage', 'Failed create companies');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getCompanies = Companies::where('id', $id)->first();
        
        $data['companies'] = $getCompanies;
        return view('companies.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getCompanies = Companies::where('id', $id)->first();

        $data['companies'] = $getCompanies;
        return view('companies.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataInputValidation = [
            'name' => $request->name,
            'logo' => $request->logo,
        ];
        $dataValidate = [
            'name' => 'required',
            'logo' => 'image|dimensions:width=100,height=100'
        ];
        $validator = Validator::make($dataInputValidation, $dataValidate);

        if ($validator->fails()) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $validator->errors());
            return redirect()->back()->withInput($request->all());
        }

        $filename = $request->old_logo;
        if (!empty($request->file('logo'))) {
            $file = $request->file('logo');
            $filename = date('YmdHis') . '-' . $file->getClientOriginalName();
            Storage::putFileAs('public', $file, $filename);
        }

        try {
            $data = [
                'name'       => $request->name,
                'email'      => $request->email,
                'logo'       => $filename,
                'website'    => $request->website,
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            Companies::where('id', $id)->update($data);
            Session::flash('flashType', 'success');
            Session::flash('flashMessage', 'Success update companies');
            return redirect()->to('/companies');
        } catch (\Throwable $th) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $th->getMessage());
            // Session::flash('flashMessage', 'Failed update companies');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Companies::where('id', $id)->delete();
            Session::flash('flashType', 'success');
            Session::flash('flashMessage', 'Success delete companies');
            return redirect()->to('/companies');
        } catch (\Throwable $th) {
            Session::flash('flashType', 'danger');
            Session::flash('flashMessage', $th->getMessage());
            // Session::flash('flashMessage', 'Failed delete companies');
            return redirect()->back()->withInput();
        }
    }
}
