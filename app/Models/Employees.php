<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'companies_id',
        'email',
        'phone',
    ];

    public function empCompanies()
    {
        return $this->hasOne(Companies::class, 'id', 'companies_id');
    }
}
